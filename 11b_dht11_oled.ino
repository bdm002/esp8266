// https://www.arduinolibraries.info/libraries/u8glib
// ESP8266 보드는  U8glib와 맞지 않는 듯하다. 아두이노용으로만 개발된 듯. 

#include "U8glib.h"
#include "DHT.h"
U8GLIB_SSD1306_128X64 u8g(U8G_I2C_OPT_NONE);

#define DHTPIN 4     // what digital pin the DHT11 is conected to
#define DHTTYPE DHT11   // there are multiple kinds of DHT sensors

DHT dht(DHTPIN, DHTTYPE);


float temperature = 0;
float humidity = 0;

String thisTemp = "";
String thisHumidity="";


void setup() {
  Serial.begin(115200);
}

void loop() {
 float h = dht.readHumidity();
 float t = dht.readTemperature();
  
  // Printing the results on the serial monitor
  Serial.print("Temperature = ");
  Serial.print(t);
  Serial.print(" *C ");
  Serial.print("    Humidity = ");
  Serial.print(h);
  Serial.println(" % ");
  
  delay(1000); // Delays 1 secods, as the DHT11 sampling rate is 1Hz

  
  u8g.firstPage(); 
do {
draw();
} while( u8g.nextPage() );
delay(1000);
}


void draw() {
u8g.setFont(u8g_font_unifont);
thisTemp = String(temperature) + "C";
const char* newTempC = (const char*) thisTemp.c_str();
u8g.drawStr( 0, 20, newTempC);

u8g.setFont(u8g_font_unifont);
thisHumidity = String(humidity) + "%";
const char* newHumidity = (const char*) thisHumidity.c_str();
u8g.drawStr( 0, 40, newHumidity);

}