#include <IRremote.h>

int RECV_PIN = 6;

IRrecv irrecv(RECV_PIN);

decode_results results;

void setup()
{
  Serial.begin(115200);
  irrecv.enableIRIn(); // Start the receiver
}

void loop() {
  if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);
    irrecv.resume(); // Receive the next value
  }
  delay(100);
}



//----------------------

// https://m.blog.naver.com/PostView.nhn?blogId=kaiserkhan21&logNo=221075466490&proxyReferer=https%3A%2F%2Fwww.google.com%2F

#include <IRremote.h>
#include <IRremoteInt.h>




#define IRPIN 6

IRrecv ir(IRPIN);
decode_results res;

void setup() {
     Serial.begin(115200);
     ir.enableIRIn();    // IR 수신 시작
     pinMode(9, OUTPUT);
     pinMode(10, OUTPUT);
     pinMode(11, OUTPUT);
   }
  
void loop() {
     if (ir.decode(&res))
     {
       switch(res.value)
      {
         case 0x4FB4AB5:    // 1
           analogWrite(11, 0);
           analogWrite(10, 0);
           analogWrite(9, 255);
           break;
         case 0x3EE5EF3F:   // 2
           analogWrite(11, 0);
           analogWrite(10, 255);
           analogWrite(9,0);
           break;
         case 0x98934743:   //3
           analogWrite(11, 255);
           analogWrite(10, 0);
           analogWrite(9,0);
           break;
         /*default :
           analogWrite(11, 2);
           analogWrite(10, 2);
           analogWrite(9,2);
           break; */
       }
       ir.resume();    //  다음 값
       
     }
   }
