#include "ESP8266WiFi.h"
#include <ESP8266WebServer.h>

#include "DHT.h"
#define DHTPIN   2   // D4
#define DHTTYPE  DHT11

DHT dht(DHTPIN, DHTTYPE);

//char *ssid = "AndroidHotspot3557";
//char *password = "";
char *ssid = "olleh_WiFi_D25A";
char *password = "0000000433";


ESP8266WebServer server(80);

void handleRoot() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  server.sendHeader("Refresh", "10");

  if (isnan(h) || isnan(t)) {
    server.send(200, "text/plain", "Error in reading sensor.");
  }
  String message = "";
  message += "Temperature : ";
  message += t;
  message += ", Humidity:  ";
  message +=h;

  server.send(200, "text/plain", message);
}

void setup(void) {
  Serial.begin(115200);

  dht.begin();

  Serial.print("Connecting to \'");
  Serial.print(ssid);
  Serial.println("\'");

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");

  Serial.print("Server IP address:  ");
  Serial.println(WiFi.localIP());

  server.on("/", handleRoot);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
}

