#include <Servo.h> 
 
Servo servo;
 
int servoPin = 5;
int angle = 0; // servo position in degrees 
int target = 180;   //target
 
void setup() 
{ 
  servo.attach(servoPin);
  servo.write(0);
} 
 
void loop() 
{ 

 for(angle = 0; angle < target; angle++) 
  { 
    servo.write(angle); 
    delay(30); 
  } 
}
