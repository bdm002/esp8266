#define PIR_PIN  7    //D7
#define NIGHT_PIN 6    //D6
#define LED_PIN  12      //D12

void setup() { 
  Serial.begin(115200);
  pinMode(PIR_PIN, INPUT);
  pinMode(NIGHT_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
 
}
 
void loop() {
int  coming = digitalRead(PIR_PIN);
int  night = digitalRead(NIGHT_PIN);

Serial.print("night=   ");
Serial.print(night);
Serial.print("\t");
Serial.println(coming);

 if (night == 1) {
  if (coming == 1) { 
    digitalWrite(LED_PIN, HIGH);
    delay(3000);
  } else {
    digitalWrite(LED_PIN, LOW);
  }
}
    digitalWrite(LED_PIN, LOW);
}
