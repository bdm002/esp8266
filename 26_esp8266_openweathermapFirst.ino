// 날씨 정보 얻기   city code, key값 알아야함
// 대전 1835235    서울 1835848  Okcheon 1839726
// 도시 찾기:   https://openweathermap.org/find?q= 
// http://openweathermap.org
//  http://api.openweathermap.org/data/2.5/weather?id=1839726&APPID=dade204398a586aee23b9f9ce3c98ffb
// api key 받기:  https://home.openweathermap.org/api_keys 
// bdm002: dade204398a586aee23b9f9ce3c98ffb
// ssh002: d870a6bb9b61beb35cf1635e6848b88b
// hsh002: 27260ae5c7028c9ad809f226c2f76d8a

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

char *ssid = "ktEgg_039";
char *password = "moda35623";

void setup(){
    Serial.begin(9600);

    WiFi.mode(WIFI_STA);
    WiFi.disconnect();

    Serial.print("Connecting to \'");
    Serial.print(ssid);
    Serial.println("\n'");

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println();
    Serial.println("WiFi connected");
}

void loop() {
  HTTPClient httpClient;

  String host = "http://api.openweathermap.org";
  String url = "/data/2.5/weather?id=1835235&APPID=";
  String key = "dade204398a586aee23b9f9ce3c98ffb";

  httpClient.begin (host + url + key);
  int httpCode = httpClient.GET();

  if (httpCode > 0) {
    Serial.printf("[HTTP] request from the client was handled: %d \n", httpCode);

    if (httpCode == HTTP_CODE_OK) {
      String payload = httpClient.getString();
      processWeatherInformation(payload);
    }

  } else {
        Serial.printf("[HTTP] connection failed: %s \n", httpClient.errorToString (httpCode).c_str());
           }
           httpClient.end();
           delay(5000);
  }
  void processWeatherInformation(String buffer) {
     int index1, index2;
     String fieldInfo;
     char *key1 = "\"weather\"", *key2 = "\"main\"", *key3 ="\"temp\"";


     index1 = buffer.indexOf(key1);
     index2 = buffer.indexOf(key2, index1);
     index1 = buffer.indexOf('\"', index2 + strlen (key2));
     index2 = buffer.indexOf('\"', index1 +1);

     fieldInfo = buffer.substring(index1+1, index2);
     Serial.println();
     Serial.println("Current Weather in Okcheon :    " + fieldInfo);

     index1 = buffer.indexOf(key3, index2);
     index2 = buffer.indexOf(':', index1 + strlen(key3));
     index1 = buffer.indexOf(',', index2 + 1);
     
     fieldInfo = buffer.substring(index2 + 1, index1);
     float temperature = fieldInfo.toFloat() - 273.15;
     Serial.print ("Current Temperature in Okcheon: ");
     Serial.println(temperature);
     Serial.println( );
}
