#include <SoftwareSerial.h>
#include <Servo.h>

SoftwareSerial BTSerial(5, 4); //(TxD2, RxD3)
int motor =2;  // D4
int LED =16;
Servo  servo;

void setup() {
  Serial.begin(9600);
  BTSerial.begin(9600); //블루투스 통신 시작
  servo.attach(motor);
  pinMode (LED, OUTPUT);
  servo.write(30);
}

void loop() {
  if(BTSerial.available())   
  {
    char bt;                
    bt = BTSerial.read();
      Serial.write(BTSerial.read());        
    if(bt == 'a')
      servo.write(0);               
      digitalWrite(LED, HIGH);
      delay(1000); 
    if(bt == 'b')
      servo.write(90);
      digitalWrite(LED, LOW);
      delay(1000);

  }
}
