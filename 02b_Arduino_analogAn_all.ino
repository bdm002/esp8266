// 시리얼 모니터  pc프로그램을 사용할 때는  \r 을 사용하고....
//  아두이노 시리얼 플로터를 사용할 때는  \n 을 사용하세요
// 쌍따옴표는 되는데 단일따옴표는 안되었음. 


void setup() {
  Serial.begin(115200);
}

void loop() {
    int a = analogRead(A0);
    int b = analogRead(A1);
    int c = analogRead(A2);
    int d = analogRead(A3);
    int e = analogRead(A4);
    int f = analogRead(A5);

   Serial.print(a);
   Serial.print(",");
   Serial.print(b);
   Serial.print(",");
   Serial.print(c);
   Serial.print("\r");  
// Serial.print("\n");


/*
    Serial.print("A2: ");
    Serial.println(c);
    Serial.print("\t");

    Serial.print("A3: ");
    Serial.println(d);
    Serial.print("\t");

    Serial.print("A4: ");
    Serial.println(e);
    Serial.print("\t");

    Serial.print("A5: ");
    Serial.println(f);
    Serial.println("\t");
*/
    
  delay(1);
}