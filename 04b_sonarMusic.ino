//  https://gcmaker.tistory.com/44

int echoPin = 4; //초음파 센서의 Echo 핀번호를 설정한다.
int trigPin = 5; //초음파 센서의 Trig 핀번호를 설정한다.
int speakerPin = 2; 
 
//49.4, 52.3, 55.4, 58.7, 62.2, 65.9, 69.9, 74, 78.4, 83.1, 88, 93.2, 
//98.8, 105, 111, 117, 124, 132, 140, 148, 157, 166, 176, 186, 
//198, 209, 222, 235, 249, 264, 
 
//int noteFreqArr[] = { //66 note
//279, 296, 314, 332, 352, 373, 
//395, 419, 444, 470, 498, 527, 559, 592, 627, 665, 704, 746, 
//790, 837, 887, 940, 996, 1050, 1110, 1180, 1250, 1320, 1400, 1490, 
//1580, 1670, 1770, 1870, 1990, 2100};
 
int noteFreqArr[] = { 527, 592, 665, 704, 790, 887, 996, 1050};
 
//int noteFreqArr[] = { //66 note 527, 592, 665, 704, 790, 887, 996, 1050};
//int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };
 
void setup(){
    Serial.begin(9600);
      pinMode(trigPin, OUTPUT);
      pinMode(echoPin, INPUT);
    // trig를 출력모드로 설정, echo를 입력모드로 설정
      pinMode(speakerPin, OUTPUT);
}
 
void loop(){
      int intdistance;
      int noteLength = 500;
      float duration, distance;
      int length = 100;
      digitalWrite(trigPin, HIGH);
      delay(100);
      digitalWrite(trigPin, LOW);
      // 초음파를 보낸다. 다 보내면 echo가 HIGH 상태로 대기하게 된다.
  
      duration = pulseIn(echoPin, HIGH); // echoPin 이 HIGH를 유지한 시간을 저장 한다.
      distance = ((float)(340 * duration) / 10000) / 2;  // HIGH 였을 때 시간(초음파가 보냈다가 다시 들어온 시간)을 가지고 거리를 계산 한다.
      intdistance = (int)distance;
      Serial.println(intdistance);
    //  Serial.println("cm");
      // 수정한 값을 출력
  
      if(intdistance <= 32.0)
      {
          //playNote(noteFreqArr[intdistance], 10);
          if((intdistance >= 0) & (intdistance <= 4))
             playTone(noteFreqArr[0], length);
          if((intdistance >= 5) & (intdistance <= 8))
             playTone(noteFreqArr[1], length);
          if((intdistance >= 9) & (intdistance <= 12))
             playTone(noteFreqArr[2], length);
          if((intdistance >= 13) & (intdistance <= 16))
             playTone(noteFreqArr[3], length);
          if((intdistance >= 17) & (intdistance <= 20))
             playTone(noteFreqArr[4], length);
          if((intdistance >= 21) & (intdistance <= 24))
            playTone(noteFreqArr[5], length);
          if((intdistance >= 25) & (intdistance <= 28))
             playTone(noteFreqArr[6], length);
          if((intdistance >= 29) & (intdistance <= 32))
             playTone(noteFreqArr[7], length);
      }
  
      //delay(10);
}
 
void playNote(int note, int duration) 
{
     char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
     //int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };
     int tones[] = { 2093,2349,2637,2793,3136,3520,3951,4186};
     // play the tone corresponding to the note name
 
     for (int i = 0; i < 30; i++) 
     {
           if (names[i] == note)  
           {
             playTone(tones[i], duration);
        }
     }
}
 
void playTone(int tone, int duration) 
{
     for (long i = 0; i < duration * 1000L; i += tone * 2) 
    {
         digitalWrite(speakerPin, HIGH);
         delayMicroseconds(tone);
         digitalWrite(speakerPin, LOW);
         delayMicroseconds(tone);
    }
}
