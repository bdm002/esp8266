// http://bit.ly/31AZxbg
// command:   AT,  AT+BAUD4   (4=9600, 8=115200), AT+NAMEname,  AT+PIN0000

#include <SoftwareSerial.h>
#define LEDPIN  13
// #define LEDPIN  16   // ESP8266 case

SoftwareSerial  BTSerial(3,2);


void setup() {
    Serial.begin(9600);
    BTSerial.begin(9600);
    pinMode(LEDPIN, OUTPUT);
    digitalWrite(LEDPIN, HIGH);
}



void  loop() {
    
    if(BTSerial.available() > 0 ) {
        Serial.write(BTSerial.read());
        digitalWrite(LEDPIN, HIGH);

    }


    if(Serial.available() > 0 ) {
    BTSerial.write(Serial.read() );
    digitalWrite(LEDPIN, LOW);
    }
}


// HC-06 BT는 기본통신속도가 대체로 9600 으로 맞추어져있다. 후에 속도변경 가능
// Arduino UNO는 softserial pin이 정해져있지않으나 다른 것은 정해져있을 수도 
// AT 키인해서 ok 라고 나오면 정상이다.
