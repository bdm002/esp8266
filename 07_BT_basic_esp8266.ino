// https://bit.ly/2JtHMUG
// command:   AT,  AT+BAUD4   (4=9600, 8=115200), AT+NAMEname,  AT+PIN0000

#include <SoftwareSerial.h>
// #define LEDPIN  13
 #define LEDPIN  16   // ESP8266 case

SoftwareSerial  BTSerial(5,4);


void setup() {
    Serial.begin(9600);
    BTSerial.begin(9600);
    pinMode(LEDPIN, OUTPUT);
    digitalWrite(LEDPIN, HIGH);
}



void  loop() {
    
    if(BTSerial.available() > 0 ) {
        Serial.write(BTSerial.read());
        digitalWrite(LEDPIN, HIGH);

    }


    if(Serial.available() > 0 ) {
    BTSerial.write(Serial.read() );
    digitalWrite(LEDPIN, LOW);
    }
}

//  ESP8266도 확실히 되는 것 확인
// USB로 PC와 연결해서 코드를 업로드 할때 문제가 발생한다. 블루투스와 USB가 같은
// RX, TX를 사용하기 때문에 업로드가 안된다. 그래서 코딩하고 업로드 할때는 블루투스를 제거하고 해야 한다.

