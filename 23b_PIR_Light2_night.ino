// 밤에 근접센서등에 접근하면 도트매트릭스가 몇 초동안 켜진다. 

#include "LedControl.h"

LedControl lc=LedControl(12,10,11,1);  // Pins: DIN,CLK,CS, # of Display connected
unsigned long delayTime=200;  // Delay between Frames

#define PIR_PIN     7        //D7
#define NIGHT_PIN   6        //D6
#define LED_PIN     13       //D13


byte symbola[] =
{
   B11111111,  // First #1
   B11111111,
   B11111111,
   B11111111,
   B11111111,
   B11111111,
   B11111111,
   B11111111
};

byte symbolb[] =
{
  B00000000, // Second  #1
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
};


void setup() { 
  Serial.begin(115200);
  pinMode(PIR_PIN, INPUT);
  pinMode(NIGHT_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
  
  lc.shutdown(0,false);  // Wake up displays
  lc.shutdown(1,false);
  lc.setIntensity(0,5);  // Set intensity levels
  lc.setIntensity(1,5);
  lc.clearDisplay(0);  // Clear Displays
  lc.clearDisplay(1);
 
}
 
void loop() {
int  coming = digitalRead(PIR_PIN);
int  night = digitalRead(NIGHT_PIN);

Serial.print("night=   ");
Serial.print(night);
Serial.print("\t");
Serial.println(coming);

 if (night == 1) {
  if (coming == 1) { 
    digitalWrite(LED_PIN, HIGH);
    DotmatrixON();  // 여기가 추가됨 
    delay(3000);
  } else {
    digitalWrite(LED_PIN, LOW);
    DotmatrixOFF();  // 여기가 추가
  }
}
    digitalWrite(LED_PIN, LOW);
    DotmatrixOFF();  // 여기가 추가
}



void DotmatrixON()
{
// Put #1 frame on both Display
    fig1a();
    delay(delayTime);

}


void DotmatrixOFF()
{
// Put #1 frame on both Display
    fig1b();
    delay(delayTime);

}


void fig1a()
{
  for (int i = 0; i < 8; i++)  
  {
    lc.setRow(0,i, symbola[i]);
  }
}

void fig1b()
{
  for (int i = 0; i < 8; i++)
  {
    lc.setRow(0,i, symbolb[i]);
  }
}

